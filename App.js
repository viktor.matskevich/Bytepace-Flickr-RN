import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import FlickrApi from "./src/api/flickr-api";
import RootNavigation from "./src/navigation/main-navigation";

export default class App extends React.Component {
  render() {
    return <RootNavigation />;
  }
}
