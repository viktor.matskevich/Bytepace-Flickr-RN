export default class FlickrApi {
  static async loadPhotos(keywords, page, per_page, callback) {
    try {
      let response = await fetch(
        "https://api.flickr.com/services/rest/?method=flickr.photos.search" +
          "&api_key=4d1adc9fc99803940d6813831ec380ad" +
          "&format=json&text=" +
          keywords +
          "&extras=url_o,url_t" +
          "&page=" +
          page +
          "&per_page=" +
          per_page
      );
      let text = await response.text();
      callback(JSON.parse(text.slice(14, -1)), null);
    } catch (error) {
      callback(null, error);
      console.error(error);
    }
  }
}
