import React from "react";
import { Share, Platform } from "react-native";

class ShareHelper {
  static share(url) {
    let subject = "Share from Bytepace-Flickr";
    var content = {
      message: "Look at it: " + url,
      title: subject
    };

    var options =
      Platform.OS === "ios" ? { subject: subject } : { dialogTitle: subject };

    Share.share(content, options);
  }
}

export default ShareHelper;
