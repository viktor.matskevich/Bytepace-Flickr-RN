import { StackNavigator } from "react-navigation";
import SearchImagesScreen from '../components/search-image/search-images-screen'
import ReviewImage from '../components/search-image/review-image'

const MainNavigator = StackNavigator({
  SearchImagesScreen: { screen: SearchImagesScreen }
})

export default RootNavigation = StackNavigator({
  SearchImagesScreen: { screen: MainNavigator },

  /** MODAL SCREENS GO HERE **/
  ReviewImage: { screen: ReviewImage }
},{
  mode: 'modal', // Remember to set the root navigator to display modally.
  headerMode: 'none', // This ensures we don't get two top bars.
})
