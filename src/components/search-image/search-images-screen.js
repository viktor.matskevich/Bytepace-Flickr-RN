import React from "react";
import { StyleSheet, View, Text } from "react-native";
import SearchBar from "./search-bar";
import FlatGridView from "./flat-grid-view";
import FlickrApi from "../../api/flickr-api";

export default class SearchImagesScreen extends React.Component {
  static navigationOptions = {
    title: "Search Images Screen"
  };

  constructor(props) {
    super(props);
    this.state = {
      title: "Input text into field above",
      images: images,
      page: 1
    };
    this.onPressImage = this._onPressImage.bind(this);
  }

  render() {
    return (
      <View style={styles.main}>
        <SearchBar
          onPress={text => {
            this.loadPhotos(text, 1);
          }}
        />
        <Text style={styles.title}>{this.state.title}</Text>
        <FlatGridView
          keywords={this.state.title}
          data={this.state.images}
          fetchPhotos={_ => {
            if (!this.state.isAllLoaded) this.fetchPhotos();
          }}
          onPressImage={photo => this._onPressImage(photo)}
        />
      </View>
    );
  }

  fetchPhotos() {
    this.loadPhotos(this.state.title, this.state.page + 1);
  }

  loadPhotos(text, page, perpage = 18) {
    if (!text) return;

    FlickrApi.loadPhotos(text, page, perpage, (items, error) => {
      if (error) {
        alert(error.toString());
      } else {
        let newData = this.getItems(items.photos.photo);
        let isAllLoaded = newData.length < 18;
        this.setState(previousState => {
          return {
            images: page > 1 ? previousState.images.concat(newData) : newData,
            title: text,
            page: page,
            isAllLoaded: isAllLoaded
          };
        });
      }
    });
  }

  getItems(data) {
    let items = [];
    for (var i = 0; i < data.length; i++) {
      let item = data[i];
      items.push({
        url_t: item.url_t,
        url_o: item.url_o,
        id: item.id,
        key: Math.floor(Math.random() * 100) + 1,
        link: "https://www.flickr.com/photos/" + item.owner + "/" + item.id
      });
    }

    return items;
  }

  _onPressImage(photo) {
    const { navigate } = this.props.navigation;
    navigate("ReviewImage", { photo: photo });
  }
}

const images = [];

const styles = StyleSheet.create({
  main: {
    flexDirection: "column",
    width: "100%",
    height: "100%",
    backgroundColor: "gray"
  },
  title: {
    width: "100%",
    height: "auto",
    padding: 5,
    textAlign: "center",
    color: "white"
  }
});
