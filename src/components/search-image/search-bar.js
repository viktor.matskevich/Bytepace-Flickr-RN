import React from "react";
import {
  StyleSheet,
  View,
  Button,
  TextInput,
  TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

export default class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: buffer.text };
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => {
            buffer.text = text;
            this.setState({ text });
          }}
          value={this.state.text}
        />
        <TouchableOpacity onPress={_ => this.props.onPress(buffer.text)}>
          <SvgUri
            width="30"
            height="30"
            source={require("../../../assets/images/ic_search.svg")}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const buffer = {
  text: ""
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    padding: 5,
    flexDirection: "row",
    width: "100%",
    height: 40
  },
  textInput: { flex: 1 },
  button: {
    width: 30,
    height: "100%"
  }
});
