import React from "react";
import { Image, StyleSheet, View, Button, Dimensions } from "react-native";
import ShareHelper from "../../utils/share-helper";
import ImageZoom from "react-native-image-pan-zoom";
import { NavigationActions } from "react-navigation";

class ReviewImage extends React.Component {
  static navigationOptions = {
    title: "Review Image"
  };

  constructor(props) {
    super(props);
    const { state } = props.navigation;
    this.state = { photo: state.params.photo };
  }

  render() {
    let photo = this.state.photo;
    let uri = photo.url_o ? photo.url_o : photo.url_t;
    let width = Dimensions.get("window").width,
      height = Dimensions.get("window").height - 70;
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <ImageZoom
          cropWidth={width}
          cropHeight={height}
          imageWidth={width}
          imageHeight={height}
        >
          <Image style={{ flex: 1 }} source={{ uri: uri }} />
        </ImageZoom>
        <Button
          title="SHARE"
          style={{ flex: 1 }}
          onPress={() => {
            ShareHelper.share(photo.link);
          }}
        />
        <Button
          title="BACK"
          style={{ flex: 1 }}
          onPress={_ =>
            this.props.navigation.dispatch(NavigationActions.back())
          }
        />
      </View>
    );
  }
}

export default ReviewImage;
