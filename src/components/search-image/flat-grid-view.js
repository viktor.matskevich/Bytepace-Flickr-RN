import React from "react";
import {
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  StyleSheet
} from "react-native";

const preProps = {
  data: [],
  numColumns: 3
};

class FlatGridView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.data ? props.data : preProps.data,
      numColumns: props.numColumns ? props.numColumns : preProps.numColumns
    };
  }

  render() {
    let data = this.props.data;

    return (
      <FlatList
        style={styles.container}
        data={data}
        renderItem={({ item }) => this._renderCell(item)}
        numColumns={this.state.numColumns}
        horizontal={false}
        onEndReached={distanceFromEnd => {
          if (data && data.length !== 0 && data.length % 18 === 0)
            this.props.fetchPhotos();
        }}
      />
    );
  }

  _renderCell(data) {
    var { height, width } = Dimensions.get("window");
    let size = width / this.state.numColumns;
    return (
      <TouchableOpacity
        style={styles.cell}
        onPress={_ => {
          this.props.onPressImage(data);
        }}
      >
        <Image
          style={{ height: size, width: size }}
          source={{
            uri: data.url_t
          }}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingEnd: 3,
    paddingTop: 3
  },
  cell: {
    flex: 1,
    marginStart: 3,
    marginBottom: 3
  }
});

export default FlatGridView;
